<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
  public function about()
  {
    $people = ['Arthur','Altergott','Victorovych'];
    return view('pages.about', compact('people'));
  }

}
